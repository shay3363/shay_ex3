#include "Vector.h"
int* copy_array(int* arr, int size)
{
	int* newArray = new int[size];
	int i = 0;
	for (i = 0; i < size; i++)
	{
		newArray[i] = arr[i];
	}
	return newArray;
}
int* resize_array(int* arr, int size, int newSize)
{
	int* newArray = new int[newSize];
	int i = 0;
	for (i = 0; i < size; i++)
	{
		newArray[i] = arr[i];
	}
	delete[] arr;
	return newArray;
}
Vector::Vector(int n)
{
	if (n < 2)
	{
		n = 2;
	}
	this->_elements = new int[n];
	this->_resizeFactor = n;
	this->_capacity = n;
	this->_size = 0;
}
Vector::~Vector()
{
	delete[] this->_elements;
}
int Vector::size() const
{
	return this->_size;
}
int Vector::capacity() const
{
	return this->_capacity;
}
int Vector::resizeFactor() const
{
	return this->_resizeFactor;
}
bool Vector::empty() const
{
	bool flag = false;
	if (this->_size == 0)
	{
		flag = true;
	}
	return flag;
}
void Vector::push_back(const int& val)
{
	int* newElements = 0;
	int i = 0;
	if (this->_size < this->_capacity)
	{
		this->_elements[this->_size] = val;
		this->_size++;
	}
	else
	{
		this->_capacity += this->_resizeFactor;
		this->_elements = resize_array(this->_elements, this->_size, this->_capacity);
		this->_elements[this->_size] = val;
		this->_size ++;
	}
}
int Vector::pop_back()
{
	int val = 0;
	if (!this->empty())
	{
		this->_size--;
		val = this->_elements[this->_size];
	}
	else
	{
		val = -9999;
		std::cout << "error: pop from empty vector"<< std::endl;
	}
	return val;
}
void Vector::reserve(int n)
{
	if (this->_capacity < n)
	{
		do
		{
			this->_capacity += this->_resizeFactor;
		} while (this->_capacity < n);
		this->_elements = resize_array(this->_elements, this->_size, this->_capacity);
	}
}
void Vector::resize(int n, const int& val)
{
	int i = 0;
	int oldSize = this->_size;
	this->reserve(n);
	this->_size = n;
	for (i = oldSize; i < n; i++)
	{
		this->_elements[i] = val;
	}
}
void Vector::resize(int n)
{
	resize(n, 0);
}
void Vector::assign(int val)
{
	int i = 0;
	for (i = 0; i < this->_size; i++)
	{
		this->_elements[i] = val;
	}
}
Vector::Vector(const Vector& other)
{
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	this->_elements = copy_array(other._elements, other._size);
}
Vector& Vector::operator=(const Vector& other)
{
	this->_capacity = other._capacity;
	this->_resizeFactor = other._resizeFactor;
	this->_size = other._size;
	delete[] this->_elements;
	this->_elements = copy_array(other._elements, other._size);
	return *this;
}
int& Vector::operator[](int n) const
{
	if (n >= this->_size)
	{
		std::cout << "error" << std::endl;
		n = 0;
	}
	return this->_elements[n];
}
void Vector::print() const
{
	int i = 0;
	for (i = 0; i < this->_size; i++)
	{
		std::cout << this->_elements[i]<< " ";
	}
}